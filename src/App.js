import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
  constructor() {
    super()
    this.state = {
      input: "",
      dict_result: "",
      newmm_result: "",
      your_text: "",
    }
  }

  handleChange = (e) => {
    this.setState({input:e.target.value})
  }

  sendInput = () => {
    axios.post('http://161.246.35.220:8000/thai-tokenize/',
      {
        text: this.state.input
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(data => {
        let dict_word = ""
        let newmm_word = ""
        data.data.result[0].map((element) => {
          dict_word = dict_word + element + " | "
        })
        this.setState({dict_result: dict_word})

        data.data.result[1].map((element) => {
          newmm_word = newmm_word + element + " | "
        })
        this.setState({newmm_result: newmm_word})
        this.setState({your_text: this.state.input})


      })
  }

  render() {
    return (
      <div className="App">
        <div class="container">
          <h1 class="col-md-12 text-center">Thai Text Tokenizer</h1>
          <div class="form-group">
            <textarea id="input" name="input" value={this.state.input} onChange={this.handleChange} class="form-control" rows="5"></textarea>
          </div>
          <div class="col-md-12 text-center">
            <button type="submit" onClick={this.sendInput} class="btn btn-info">Submit</button>
          </div>
          <hr/>

          <h2>Input Text</h2>
          <div class="alert alert-primary" role="alert">{this.state.your_text}</div>
          <hr/>
          <h2>Result</h2>
          <strong>From Dict Engine</strong>
          <div class="alert alert-info" role="alert">{this.state.dict_result}</div>
          <hr/>
          <strong>From New-MM Engine</strong>
          <div class="alert alert-info" role="alert">{this.state.newmm_result}</div>
        </div>
      </div>
    );
  }
}

export default App;
